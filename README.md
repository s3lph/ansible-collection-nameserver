# Ansible Collection - s3lph.nameserver

Authoritative nameserver setup using the [knot][knot] DNS server.

For a usage example see `docs/`.

[knot]: https://www.knot-dns.cz/